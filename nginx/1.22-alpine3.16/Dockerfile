# ------------------------------------------------------------------------------
# 基于 Alpine Linux 系统, 构建 Nginx 镜像。
# Alpine Linux 版本: 3.16.1
# Nginx stable 版本: 1.22.0
# 官方 Docker Hub: https://hub.docker.com/_/nginx
# 官方 Docker GitHub: https://github.com/nginxinc/docker-nginx
# 修改了以下内容:
#   1.修改软件源为阿里云镜像源.
#   2.修改系统时区为上海(Asia/Shanghai).
#   3.修改系统语言为 zh_CN.UTF-8.
# 快速使用:
# docker pull registry.cn-hangzhou.aliyuncs.com/goshawk/nginx:1.22-alpine3.16
# ------------------------------------------------------------------------------

FROM nginx:1.22.0-alpine

ENV TIME_ZONE=Asia/Shanghai LANG='zh_CN.UTF-8' LANGUAGE='zh_CN:zh' LC_ALL='zh_CN.UTF-8'

RUN sed -i 's#dl-cdn.alpinelinux.org#mirrors.aliyun.com#g' /etc/apk/repositories; \
    apk update && apk add --no-cache musl-locales musl-locales-lang; \
    cp /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime && echo ${TIME_ZONE} > /etc/timezone; \
    apk del tzdata && rm -rf /var/cache/apk/* /tmp/* /root/.cache;

RUN echo Verifying ... \
    && echo Current Date: `date` \
    && echo Current Lang: ${LANG} \
    && echo Nginx version: && nginx -v \
    && echo Complete.