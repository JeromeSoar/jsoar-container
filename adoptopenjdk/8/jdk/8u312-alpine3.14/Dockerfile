# ------------------------------------------------------------------------------
# 基于 Alpine Linux 系统, 构建 AdoptOpenJDK 镜像。
# Alpine Linux 版本: 3.14
# JDK 版本: jdk8u312-b07
# JDK 安装包: https://github.com/adoptium/temurin8-binaries/releases/jdk8u312-b07
# 镜像维护内容:
#   1.修改系统语言为简体中文(zh_CN)。
#   2.修改系统时区为上海(Asia/Shanghai)。
# 快速使用:
# docker pull registry.cn-hangzhou.aliyuncs.com/goshawk/adoptopenjdk:8u312-jdk-alpine3.14
# docker pull registry.cn-hangzhou.aliyuncs.com/goshawk/adoptopenjdk:8-jdk-alpine
# ------------------------------------------------------------------------------

FROM alpine:3.14

ENV TIME_ZONE=Asia/Shanghai LOCALE='zh_CN' LANG='zh_CN.UTF-8' LANGUAGE='zh_CN:zh' LC_ALL='zh_CN.UTF-8'

RUN sed -i 's#dl-cdn.alpinelinux.org#mirrors.aliyun.com#g' /etc/apk/repositories; \
    apk add --no-cache tzdata; \
    cp /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime && echo ${TIME_ZONE} > /etc/timezone; \
    GLIBC_VER="2.33-r0"; \
    ALPINE_GLIBC_REPO="https://github.com/sgerrand/alpine-pkg-glibc/releases/download"; \
    wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub; \
    wget -q -O /tmp/glibc-${GLIBC_VER}.apk ${ALPINE_GLIBC_REPO}/${GLIBC_VER}/glibc-${GLIBC_VER}.apk; \
    wget -q -O /tmp/glibc-bin-${GLIBC_VER}.apk ${ALPINE_GLIBC_REPO}/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk; \
    wget -q -O /tmp/glibc-i18n-${GLIBC_VER}.apk ${ALPINE_GLIBC_REPO}/${GLIBC_VER}/glibc-i18n-${GLIBC_VER}.apk; \
    apk add --no-cache /tmp/glibc-${GLIBC_VER}.apk /tmp/glibc-bin-${GLIBC_VER}.apk /tmp/glibc-i18n-${GLIBC_VER}.apk; \
    /usr/glibc-compat/bin/localedef -i $LOCALE -f UTF-8 $LANG; \
    echo "export LANG=$LANG" > /etc/profile.d/locale.sh; \
    apk del tzdata && rm -rf /var/cache/apk/* /tmp/* /root/.cache;


ENV JAVA_VERSION=jdk8u312-b07 JAVA_HOME=/opt/java/openjdk PATH="/opt/java/openjdk/bin:$PATH"

RUN set -eux; \
    ARCH="$(apk --print-arch)"; \
    case "${ARCH}" in \
       amd64|x86_64) \
         SHA='699981083983b60a7eeb511ad640fae3ae4b879de5a3980fe837e8ade9c34a08'; \
         BINARY_URL='https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u312-b07/OpenJDK8U-jdk_x64_linux_hotspot_8u312b07.tar.gz'; \
         ;; \
       *) \
         echo "Unsupported arch: ${ARCH}"; \
         exit 1; \
         ;; \
    esac; \
    wget -q -O /tmp/openjdk.tar.gz ${BINARY_URL}; \
    echo "${SHA} */tmp/openjdk.tar.gz" | sha256sum -c -; \
    mkdir -p ${JAVA_HOME}; \
    tar -xf /tmp/openjdk.tar.gz -C ${JAVA_HOME} --strip-components=1; \
    rm -rf /tmp/openjdk.tar.gz;

RUN echo Verifying ... \
    && echo java -version && java -version \
    && echo Complete.